package com.epam.rd.autotasks;

class Spiral {
    static int[][] spiral(int rows, int columns) {

        int[][] returnSpiral = new int[rows][columns];
        int arrayFiller = 1;

        // Defining the boundaries of the matrix.
        int top = 0, bottom = rows - 1, left = 0, right = columns - 1;

        // Defining the direction in which the array is to be traversed.
        int dir = 1;

        while (top <= bottom && left <= right) {

            if (dir == 1) {    // moving left->right
                for (int i = left; i <= right; ++i) {
                    returnSpiral[top][i] = arrayFiller++;
                }
                // Since we have traversed the whole first
                // row, move down to the next row.
                ++top;
                dir = 2;
            }
            else if (dir == 2) {     // moving top->bottom
                for (int i = top; i <= bottom; ++i) {
                    returnSpiral[i][right] = arrayFiller++;
                }
                // Since we have traversed the whole last
                // column, move left to the previous column.
                --right;
                dir = 3;
            }
            else if (dir == 3) {     // moving right->left
                for (int i = right; i >= left; --i) {
                    returnSpiral[bottom][i] = arrayFiller++;
                }
                // Since we have traversed the whole last
                // row, move up to the previous row.
                --bottom;
                dir = 4;
            }
            else if (dir == 4) {     // moving bottom->up
                for (int i = bottom; i >= top; --i) {
                    returnSpiral[i][left] = arrayFiller++;
                }
                // Since we have traversed the whole first
                // col, move right to the next column.
                ++left;
                dir = 1;
            }
        }
        return returnSpiral;
    }
}
